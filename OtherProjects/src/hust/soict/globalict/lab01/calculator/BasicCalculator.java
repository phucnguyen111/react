package hust.soict.globalict.lab01.calculator;
import javax.swing.JOptionPane;
public class BasicCalculator{
    public static void main(String[] args) {
        String strNum1, strNum2;
        String strNotification = "You've just entered: ";
        String strNotificationSum="The Sum is: ";
        String strNotificationDifference="The Difference is: ";
        String strNotificationProduct="The Product is: ";
        String strNotificationQuotient="The Quotient is: ";
        String divisor;
        

        //Initialize the input



        strNum1 = JOptionPane.showInputDialog(null,"Please Input the first number :","Input the first number",JOptionPane.INFORMATION_MESSAGE);

        strNotification += strNum1 + " and ";

        strNum2 = JOptionPane.showInputDialog(null,"Please Input the second number :","Input the second number",JOptionPane.INFORMATION_MESSAGE);

        do{
        divisor=JOptionPane.showInputDialog(null,"Please Input the number you select as the divisor ( must be the first or the second number ) :","Input the divisor",JOptionPane.INFORMATION_MESSAGE);
        
    
        }while(!divisor.equals(strNum1) && !divisor.equals(strNum2)); //check if divisor is num1 or num2
        strNotification += strNum2 +"\n"+"The divisor is "+divisor;

        //Converting String to Double

        double divisor1=Double.parseDouble(divisor);
        double num1=Double.parseDouble(strNum1);
        double num2=Double.parseDouble(strNum2);
        double sum, difference, product, quotient;
       

        //Calculation

        sum=num1+num2;
        product=num1*num2;
        if(num1>num2){
        difference=num1-num2;
        }
        else difference=num2-num1;
        if(divisor1==num1){
            quotient=num2/num1;
        }
        else quotient=num1/num2;


        //Converting Double to String (unnessary)
        /*

        String sum1=Double.toString(sum);
        String difference1=Double.toString(difference);
        String product1=Double.toString(product);
        String quotient1=Double.toString(quotient);

        */

        strNotificationSum+=sum;
        strNotificationDifference+=difference;
        strNotificationProduct+=product;
        strNotificationQuotient+=quotient;

        

        JOptionPane.showMessageDialog(null,strNotification,"Show the Input", JOptionPane.INFORMATION_MESSAGE);
        JOptionPane.showMessageDialog(null,strNotificationSum+"\n"+strNotificationDifference+"\n"+strNotificationProduct+"\n"+strNotificationQuotient,"Show the Result", JOptionPane.INFORMATION_MESSAGE);


        System.exit(0);
        
    }
    
}