package hust.soict.globalict.lab02.daysOfAMonth;

import javax.swing.JOptionPane;
public class DaysOfAMonth{
	static int checkDay(String Month, String Year){
		int iYear = Integer.parseInt(Year);
		if((Month.equals("1") || Month.equals("January") || Month.equals("Jan.") || Month.equals("Jan"))==true && (iYear>0)){
			return 1;
		}
		else if((Month.equals("2") || Month.equals("February") || Month.equals("Feb.") || Month.equals("Feb"))==true && (iYear>0)){
			return 2;
		}
		else if((Month.equals("3") || Month.equals("March") || Month.equals("Mar.") || Month.equals("Mar"))==true && (iYear>0)){
			return 3;
		}
		else if((Month.equals("4") || Month.equals("April") || Month.equals("Apr.") || Month.equals("Apr"))==true && (iYear>0)){
			return 4;
		}
		else if((Month.equals("5") || Month.equals("May") || Month.equals("May.") || Month.equals("May"))==true && (iYear>0)){
			return 5;
		}
		else if((Month.equals("6") || Month.equals("June") || Month.equals("Jun.") || Month.equals("Jun"))==true && (iYear>0)){
			return 6;
		}
		else if((Month.equals("7") || Month.equals("July") || Month.equals("Jul.") || Month.equals("Jul"))==true && (iYear>0)){
			return 7;
		}
		else if((Month.equals("8") || Month.equals("August") || Month.equals("Aug.") || Month.equals("Aug"))==true && (iYear>0)){
			return 8;
		}
		else if((Month.equals("9") || Month.equals("September") || Month.equals("Sep.") || Month.equals("Sep"))==true && (iYear>0)){
			return 9;
		}
		else if((Month.equals("10") || Month.equals("October") || Month.equals("Oct.") || Month.equals("Oct"))==true && (iYear>0)){
			return 10;
		}
		else if((Month.equals("11") || Month.equals("November") || Month.equals("Nov.") || Month.equals("Nov"))==true && (iYear>0)){
			return 11;
		}
		else if((Month.equals("12") || Month.equals("December") || Month.equals("Dec.") || Month.equals("Dec"))==true && (iYear>0)){
			return 12;
		}
		else{
			return 0;
		}

	}

	static int checkLeapYear(String Year){
		int year=Integer.parseInt(Year);
		if(year % 4 == 0) {
			if (year % 100 == 0) {
				if (year % 400 == 0)
					return 1;
				else
					return 0;
			}
			else
				return 1;
		}else
			return 0;
	}

	static int showDays(String Month, String Year){
		
		if(checkDay(Month, Year)==1){
			return 31;
		}
		else if(checkDay(Month, Year)==2 && (checkLeapYear(Year)==0)){
			return 28;
		}
		else if(checkDay(Month, Year)==3){
			return 31;
		}
		else if(checkDay(Month, Year)==4){
			return 30;
		}
		else if(checkDay(Month, Year)==5){
			return 31;
		}
		else if(checkDay(Month, Year)==6){
			return 30;
		}
		else if(checkDay(Month, Year)==7){
			return 31;
		}
		else if(checkDay(Month, Year)==8){
			return 31;
		}
		else if(checkDay(Month, Year)==9){
			return 30;
		}
		else if(checkDay(Month, Year)==10){
			return 31;
		}
		else if(checkDay(Month, Year)==11){
			return 30;
		}
		else if(checkDay(Month, Year)==12){
			return 31;
		}
		else if(checkDay(Month, Year)==2 && (checkLeapYear(Year)==1)){
			return 29;
		}
		else{
			return 0;
		}

	}
	public static void main(String args[]) {
        String Month;
		String Year;
		int Day;
		String cont;
		int iCont;
		do{

		do{

        Month = JOptionPane.showInputDialog(null, "Enter Month (e.g: 1, January, Jan., Jan are all OK):","Input Month",JOptionPane.INFORMATION_MESSAGE );
		Year = JOptionPane.showInputDialog(null, "Enter Year (e.g: 1999 is OK but 99 is not):","Input Year",JOptionPane.INFORMATION_MESSAGE );
		if(checkDay(Month, Year)==0){
			JOptionPane.showMessageDialog(null,"Invalid Month & Year. Please re-enter","Warning",JOptionPane.INFORMATION_MESSAGE);
		}
		}while(checkDay(Month,Year)==0);
	    
		

		JOptionPane.showMessageDialog(null, "You have entered: "+"Month: "+Month+" Year: "+Year, "Show the Month-Year", JOptionPane.INFORMATION_MESSAGE);

		Day=showDays(Month, Year);
		JOptionPane.showMessageDialog(null, "The number of days in your entered month-year is: "+Day+" days","Show the result", JOptionPane.INFORMATION_MESSAGE);
		cont=JOptionPane.showInputDialog(null,"Press 1 to continue, others to Exit", "Continue or Not", JOptionPane.INFORMATION_MESSAGE);
		iCont=Integer.parseInt(cont);
		}while(iCont==1);
		System.exit(0);
	}
}