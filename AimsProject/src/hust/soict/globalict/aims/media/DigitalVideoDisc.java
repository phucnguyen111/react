package hust.soict.globalict.aims.media;
import java.util.Arrays;
import java.util.List;


public class DigitalVideoDisc extends Media {
	
	private String director;
	private int length;
	
	
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	
	
	//constructor
	
	public DigitalVideoDisc(String title) {
		super(title);
	}
	
	public DigitalVideoDisc(String title, String category) {
		super(title,category);
	
	}
	
	public DigitalVideoDisc(String title, String category, String director) {
		
		super(title,category);
		this.director = director;
	}

	public DigitalVideoDisc(String title, String category, float cost){
		super(title, category, cost);
	}
	
	public DigitalVideoDisc(String id,String title, String category, String director, int length, float cost) {
		
		super(id,title,category,cost);
		this.length = length;
		this.director = director;
	}
	public DigitalVideoDisc(String id,String title, String category, float cost){
		super(id,title, category, cost);
	}
	
	public boolean search(String title) {
		
		boolean foundToken = false;
		String inputToken[] = title.toLowerCase().split(" ");
		String discTitle[] = this.getTitle().toLowerCase().split(" ");

		List<String> inputTokenList = Arrays.asList(inputToken);
		List<String> discTitleList = Arrays.asList(discTitle);
		
		if(discTitleList.containsAll(inputTokenList)){
			foundToken = true;
		}
		
		return foundToken;
	}
	
	
}
