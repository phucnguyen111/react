package hust.soict.globalict.lab02.starTriangle;
import java.util.Scanner;
public class StarTriangle {
	private static Scanner keyboard;
	public static void main(String[] args) {
		keyboard = new Scanner(System.in);
		
		System.out.println("Enter the height of n stars:");
		int n = keyboard.nextInt();
		for (int i=0; i<n; i++) 
        { 
  
            for (int j=n-i; j>1; j--) 
            { 
                
                System.out.print(" "); 
            } 
   
            for (int j=0; j<=i; j++ ) 
            { 
                
                System.out.print("* "); 
            } 
 
            System.out.println(); 
        } 
		System.exit(0);
	}
}
