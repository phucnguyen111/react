package hust.soict.globalict.aims.media;
import java.util.List;
import java.util.ArrayList;

public class Book extends Media {
	
	private List<String> authors = new ArrayList<String>();
	
	public List<String> getAuthors() {
		return authors;
	}
	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	//constructor
	public Book(String title) {
		
		super(title);
	}
	public Book(String title, String category){
		super(title, category);
	}
	public Book(String id,String title, String category, List<String> authors, float cost){
		super(id,title, category,cost);
		this.authors = authors;
	}
	public Book(String title, String category, float cost){
		super(title, category, cost);
	}
	public Book(String id,String title, String category, float cost){
		super(id,title, category, cost);
	}

	
	
	public boolean addAuthor(String authorName) {
		if(authors.contains(authorName) == false) {
			authors.add(authorName);
			return true;
		}
		return false;
	}
	
	public boolean removeAuthor(String authorName) {
		if(authors.contains(authorName) == true) {
			authors.remove(authorName);
			return true;
		}
		return false;
	}
}
