package hust.soict.globalict.aims;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.order.Order;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
public class Aims {
	Order anOrder; 
	public Order getAnOrder(){
		return anOrder;
	}
	public void setAnOrder(Order anOrder){
		this.anOrder = anOrder;
	}
	public static void showMenu() {
		System.out.println("\nOrder Management Application:");
		System.out.println("-----------------------------------");
		System.out.println("1. Create new order.");
		System.out.println("2. Add item to the order.");
		System.out.println("3. Delete item by id.");
		System.out.println("4. Display the item list of order.");
		System.out.println("0. Exit.");
		System.out.println("Please choose a number 0-1-2-3-4:");
		System.out.println("------------------------------------");
	}
	
	public static void main(String[] args) {
		
		Aims aims = new Aims();
		

		Media dvd1 = new DigitalVideoDisc("dvd1","Toy Story 4","Animation","Josh Cooley",100,11.97f);
		Media dvd2 = new DigitalVideoDisc("dvd2","The Hunger Games","Science Fiction","Gary Ross",142,20.14f);
		Media dvd3 = new DigitalVideoDisc("dvd3","Parasite","Black Comedy","Bong Joon-ho",132,18.53f);
		Media dvd4 = new DigitalVideoDisc("dvd4","Star Wars","Sciecne Fiction","George Lucas",124,29.69f);
		Media dvd5 = new DigitalVideoDisc("dvd5","Titanic","Love Tragedy","James Cameron",203,23.15f);
		Media dvd6 = new DigitalVideoDisc("dvd6","Chi Tro Ly Cua Anh","Love Comedy","Mira Duong",135,17.76f);
		Media dvd7 = new DigitalVideoDisc("dvd7","Hai Phuong","Action","Ngo Thanh Van",211,20.65f);
		Media dvd8 = new DigitalVideoDisc("dvd8","Mat Biec","Romance","Victor Vu",117,33.55f);
		Media dvd9 = new DigitalVideoDisc("dvd9","Us","Horror","Jordan Peele",220,38.16f);
		Media dvd10 = new DigitalVideoDisc("dvd10","Onward","Animation","Dan Scanlon",222,27.35f);

		Media DVDArray[] = {dvd1,dvd2,dvd3,dvd4,dvd5,dvd6,dvd7,dvd8,dvd9,dvd10};
		List<Media> DVDList = Arrays.asList(DVDArray); 
		
		Media book1 = new Book("book1", "Kinh Van Hoa", "Children",5.3f);
		Media book2 = new Book("book2", "Harry Potter", "Novel",15.3f);
		Media book3 = new Book("book3", "Doraemon", "Manga",4.5f);
		Media book4 = new Book("book4", "Lao Hac", "Vietnamese novel",10.8f);
		Media book5 = new Book("book5", "Gone with the wind", "Novel",17.5f);
		Media book6 = new Book("book6", "Algebra I", "Scientific",3.4f);

		Media BookArray[] = {book1,book2,book3,book4,book5,book6};
		List<Media> BookList = Arrays.asList(BookArray);

		String [] IdArray = {"dvd1","dvd2","dvd3","dvd4","dvd5","dvd6","dvd7","dvd8","dvd9","dvd10","book1","book2","book3","book4","book5","book6"};
		List<String> AllId = Arrays.asList(IdArray);
			System.out.println("List of available items:");
			System.out.println("-----------------------------------------");
			System.out.format("%-10s%-20s\n", "Id","Title" );
			System.out.println("-----------------------------------------");
			for(int i =0;i<DVDList.size();i++){
				System.out.format("%-10s%-20s\n", DVDList.get(i).getId(),DVDList.get(i).getTitle());
	
			}
			for(int i =0;i<BookList.size();i++){
				System.out.format("%-10s%-20s\n", BookList.get(i).getId(),BookList.get(i).getTitle());
	
			}
			System.out.println("----------------------------------------");

		Scanner kb = new Scanner(System.in);
		int menu = 0;
		do{
			showMenu();
			System.out.println("Enter your choice:");
			menu = kb.nextInt();
			kb.nextLine();
			switch(menu){
				case 1:
					Order newOrder = new Order();
					aims.setAnOrder(newOrder);
					System.out.println("Number of orders: "+aims.getAnOrder().getNbOrders());
					break;
				case 2:
					System.out.println("Enter item id (e.g: dvd1, book2) to add item:");
					String itemIdAdd = kb.nextLine();
					if(AllId.parallelStream().anyMatch(itemIdAdd::contains)){
						for(int i = 0;i < DVDList.size();i++){
							if(itemIdAdd.equals(DVDList.get(i).getId())){
								
								aims.getAnOrder().addMedia(DVDList.get(i));
								System.out.println("DVD "+"\'"+DVDList.get(i).getTitle()+"\'"+" has been successfully added to order");
							}
						}
						for(int j = 0;j < BookList.size();j++){
							if(itemIdAdd.equals(BookList.get(j).getId())){
								aims.getAnOrder().addMedia(BookList.get(j));
								System.out.println("Book "+"\'"+BookList.get(j).getTitle()+"\'"+" has been successfully added to order");
							}
						}
					}
					else{
						System.out.println("Wrong item Id! Try again");
					}
					break;
				case 3:
					System.out.println("Enter item id (e.g: dvd1, book2) to remove item:");
					String itemIdRemove = kb.nextLine();
					if(AllId.parallelStream().anyMatch(itemIdRemove::contains)){
						for(int i = 0;i < DVDList.size();i++){
							if(itemIdRemove.equals(DVDList.get(i).getId())){
								boolean check = aims.getAnOrder().removeMedia(itemIdRemove);
								if(check== true){
									System.out.println("DVD "+"\'"+DVDList.get(i).getTitle()+"\'"+" has been successfully removed from order");
								}
								else{
									System.out.println("This DVD is not in your order! Canot remove!");
								}
							}
						}
						for(int j = 0;j < BookList.size();j++){
							if(itemIdRemove.equals(BookList.get(j).getId())){
								boolean check = aims.getAnOrder().removeMedia(itemIdRemove);
								if(check == true){
									System.out.println("Book "+"\'"+BookList.get(j).getTitle()+"\'"+" has been successfully removed from order");
								}
								else{
									System.out.println("This book is not in your order! Canot remove!");
								}
							}
						}
					}
					else{
						System.out.println("Wrong item Id! Try again");
					}
					break;
				case 4:
					aims.getAnOrder().printOrder();
					break;
				case 0:
					System.out.println("Bye ! Exitting ...");
					break;
				default:
					System.out.println("Invalid value! Try again later");
				
			}
		}while(menu!=0);
		kb.close();
		
	}

}
