
public class DateTest{
    public static void main(String[] args){
        MyDate currentDate = new MyDate();
        System.out.println("The current date is:");
        currentDate.print();
    

        MyDate iDate = new MyDate(1,11,1999); // integer Date MyDate(day,month,year)
        System.out.println("This is my birthday in dd/mm/yyyy format: ");
        iDate.print();

        //User enter Date format eg Feb 19th 2020
        
        String inputDate = MyDate.accept();
        MyDate date = new MyDate(inputDate);
        if(date.checkDay(date.getDay(),date.getMonth(),date.getYear()) == 1){
        System.out.println("You've entered: ");
        date.print();
        }


    }
}