public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	
	private static int qtyOrdered = 0;

	public int getQtyOrdered() {
		return qtyOrdered;
	}

	public void setQtyOrdered(int qtyOrdered) {
		Order.qtyOrdered = qtyOrdered;
	}
	public int findDVDindex(DigitalVideoDisc disc, int nOrdered) {
		int i;
		for(i = 0; i < nOrdered; i++) {
			if(itemsOrdered[i].equals(disc)) {
				return i;
			}
		}
		return -1;
	}
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(qtyOrdered == MAX_NUMBERS_ORDERED){
			System.out.println("The maximum number of items reached ( 10 items ) ! ");
		}
		else if(qtyOrdered < MAX_NUMBERS_ORDERED){
			itemsOrdered[qtyOrdered] = disc;
			qtyOrdered++;
			System.out.println("\'"+itemsOrdered[qtyOrdered-1].getTitle()+"\'"+" has been successfully added to your order !\n");
		}
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		int DVDindex = findDVDindex(disc, qtyOrdered);
		if(DVDindex  >=0 ) {
			System.out.println("\n\'"+itemsOrdered[DVDindex].getTitle()+"\'"+" has been successfully removed from your order !");

			for(int i = DVDindex; i <= qtyOrdered; i++) {
				itemsOrdered[i] = itemsOrdered[i+1];

			}
			qtyOrdered--;

		}
		else if(DVDindex == -1) {
			System.out.println("This DVD has not been added in your order ! Cannot remove");
		}
		else if(qtyOrdered == 0) {
			System.out.println("Your order is empty ! Cannot remove");
		}
		
	}
	
	public float totalCost() {
		float cost = 0.0f;
		for(int i = 0; i< qtyOrdered; i++) {
			cost += itemsOrdered[i].getCost();
		}
		return cost;
	}

	public void printOrder() {
		System.out.println("\n****************************************Your Order********************************************");
		System.out.println("______________________________________________________________________________________________");
		System.out.format("\n%-5s%-25s%-20s%-20s%-15s%-15s\n","No", "Title", "Category","Director","Length(min)","Cost($)");
		System.out.println("______________________________________________________________________________________________\n");
		
		for(int i = 0; i < qtyOrdered; i++) {
			System.out.format("%-5d%-25s%-20s%-20s%-15d%-15.2f\n",i+1, itemsOrdered[i].getTitle(), itemsOrdered[i].getCategory(),itemsOrdered[i].getDirector(),itemsOrdered[i].getLength(), itemsOrdered[i].getCost() );
			
		}
		System.out.println("______________________________________________________________________________________________\n");
		System.out.println("\n**********************************************************************************************");


		System.out.println("\nThe total number of items in your order is  " + qtyOrdered);
		System.out.println("\nYour total cost is: "+"$"+ totalCost());
		System.out.println("\nThanks for shopping !\n");
	}
}
