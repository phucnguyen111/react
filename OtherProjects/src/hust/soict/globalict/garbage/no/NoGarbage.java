package hust.soict.globalict.garbage.no;

import java.io.IOException;
import java.util.Scanner;
import java.io.File;

public class NoGarbage {
    public static void main(String[] args){
        StringBuffer garbage = new StringBuffer();
        File txtFile = new File(System.getProperty("user.dir")+"//src//hust//soict//globalict//garbage//garbage.txt");
        try{
            Scanner kb = new Scanner(txtFile);
            while(kb.hasNextLine() == true){
            garbage.append(kb.nextLine());
            }
            System.out.println("Complete reading file");
            kb.close();
        } 
        catch (IOException e){
            System.out.println("Too much garbage");
            e.printStackTrace();
        }
        

    }
}