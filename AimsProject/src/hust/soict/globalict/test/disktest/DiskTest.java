package hust.soict.globalict.test.disktest;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.order.Order;
import java.util.Scanner;
import java.util.Arrays;
import java.util.List;

public class DiskTest {
    public static void main(String[] args) {
		
		Order anOrder = new Order();
		
		Media dvd1 = new DigitalVideoDisc("dvd1","Toy Story 4","Animation",11.97f);
		Media dvd2 = new DigitalVideoDisc("dvd2","The Hunger Games","Science Fiction",20.14f);
		Media dvd3 = new DigitalVideoDisc("dvd3","Toy Story","Animation",20.31f);
		Media dvd4 = new DigitalVideoDisc("dvd4","Toy Story 2","Animation",17.55f);
		Media dvd5 = new DigitalVideoDisc("dvd5","Toy Story 3","Animation",18.97f);
		Media dvd6 = new DigitalVideoDisc("dvd6","Chi Tro Ly Cua Anh","Love Comedy",17.76f);
		Media dvd7 = new DigitalVideoDisc("dvd7","Hai Phuong","Action",20.65f);
		Media dvd8 = new DigitalVideoDisc("dvd8","Mat Biec","Romance",33.55f);
		Media dvd9 = new DigitalVideoDisc("dvd9","The nun","Horror",38.16f);
		Media dvd10 = new DigitalVideoDisc("dvd10","Onward","Animation",27.35f);
        

		Media MediaArray[] = {dvd1,dvd2,dvd3,dvd4,dvd5,dvd6,dvd7,dvd8,dvd9,dvd10};
		List<Media> MediaList = Arrays.asList(MediaArray); 
		
		anOrder.addMediaList(MediaList);
		// test lucky item
		anOrder.printOrder();
	
		// test search

		Scanner kb = new Scanner(System.in);
		System.out.print("Enter title to search for the disk: ");
		String inputTitle = kb.nextLine(); 
		
		for(Media disc: MediaList){
			if(disc.search(inputTitle) == true){
				System.out.println("DVD "+"\'"+disc.getTitle()+"\'"+" contains the input tokens");
			}
			
		}
		if((dvd1.search(inputTitle) == false && dvd2.search(inputTitle) == false && dvd3.search(inputTitle) == false && dvd4.search(inputTitle) == false && dvd5.search(inputTitle) == false && dvd6.search(inputTitle) == false
		&& dvd7.search(inputTitle) == false && dvd8.search(inputTitle) == false && dvd9.search(inputTitle) == false && dvd10.search(inputTitle) == false )){
			System.out.println("There are no DVDs containing the input tokens");
		}
		
		kb.close();

		
	}
}