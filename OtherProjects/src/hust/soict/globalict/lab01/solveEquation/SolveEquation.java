package hust.soict.globalict.lab01.solveEquation;
import javax.swing.JOptionPane;
import java.lang.Math;
public class SolveEquation{

    //Solve linear equation

    static void linearEquation(String a, String b){
        
        double a1=Double.parseDouble(a);
        double b1=Double.parseDouble(b);
        double ans1;
        ans1=-b1/a1;
        String printAnswer="The solution to the linear equation: "+a+"x"+" + "+b+" = 0 is: x = "+ans1;
        JOptionPane.showMessageDialog(null,printAnswer,"Show Solution", JOptionPane.INFORMATION_MESSAGE);
    }

    //Solve linear system

    static void linearSystem(String a11, String a12, String a21, String a22, String b1, String b2){
        double a11d=Double.parseDouble(a11);
        double a12d=Double.parseDouble(a12);
        double a21d=Double.parseDouble(a21);
        double a22d=Double.parseDouble(a22);
        double b1d=Double.parseDouble(b1);
        double b2d=Double.parseDouble(b2);
        double ans1,ans2;
        double d,d1,d2;
        String printAnswer="The system has";

        d=a11d*a22d-a21d*a12d;
        d1=b1d*a22d-b2d*a12d;
        d2=a11d*b2d-a21d*b1d;

        if(d==0){
            printAnswer+=" infinitely many solutions if Dx = Dy = D = 0";
            JOptionPane.showMessageDialog(null,printAnswer,"Show Solution", JOptionPane.INFORMATION_MESSAGE);
          
        }

        else if(d!=0){
            ans1=d1/d;
            ans2=d2/d;
            printAnswer+=" a unique solution ( x1 , x2 ) = " +"( "+ans1+" , "+ans2+" )";
            JOptionPane.showMessageDialog(null,printAnswer,"Show Solution", JOptionPane.INFORMATION_MESSAGE);
        }

        else{
            JOptionPane.showMessageDialog(null,printAnswer+" no solution ","Show Solution", JOptionPane.INFORMATION_MESSAGE);
        }

    }

    //Solve 2nd degree equation

    static void secondDegreeEquation(String a3, String b3, String c3){
        double a3d=Double.parseDouble(a3);
        double b3d=Double.parseDouble(b3);
        double c3d=Double.parseDouble(c3);
        double delta;
        double ans3,ans31,ans32;
        String printAnswer="The equation has";

        delta=b3d*b3d-4*a3d*c3d;

        if(delta==0){
            ans3=-b3d/(2*a3d);
            printAnswer+=" double root x1 = x2 = "+ans3;
            JOptionPane.showMessageDialog(null,printAnswer,"Show Solution", JOptionPane.INFORMATION_MESSAGE);
        }

        else if(delta>0){
            ans31=(-b3d+Math.sqrt(delta))/(2*a3d);
            ans32=(-b3d-Math.sqrt(delta))/(2*a3d);
            printAnswer+=" two distinct roots: "+"x1 = "+ans31+" and x2 = "+ans32;
            JOptionPane.showMessageDialog(null,printAnswer,"Show Solution", JOptionPane.INFORMATION_MESSAGE);
        }

        else{
            JOptionPane.showMessageDialog(null,printAnswer+" no solution ","Show Solution", JOptionPane.INFORMATION_MESSAGE);

        }

    }

    
    public static void main(String[] args) {
        String a,b; 
        String a11,a12,a21,a22,b1,b2;
        String a3,b3,c3;
        String selectOption;
        int menu;

        //Menu

        do{

        selectOption=JOptionPane.showInputDialog(null,"Please Choose the type of equation you want to solve \n1.First-degree equation with one variable: ax + b = 0 \n2.The system of first-degree equations with two variables\n \ta11.x1 + a12.x2 = b1 and \n \ta21.x1 + a22.x2 = b2 \n3.The second-degree equation with one variable: ax^2 + bx + c = 0\n4.Exit","Menu",JOptionPane.INFORMATION_MESSAGE);
        menu=Integer.parseInt(selectOption);
        switch(menu){
            case 1:
                a=JOptionPane.showInputDialog(null,"Please Input a:","Input the first parameter",JOptionPane.INFORMATION_MESSAGE);
                b=JOptionPane.showInputDialog(null,"Please Input b:","Input the second parameter",JOptionPane.INFORMATION_MESSAGE);
                linearEquation(a, b);
                break;
            
            case 2:
                a11=JOptionPane.showInputDialog(null,"Please Input a11:","Input the first parameter",JOptionPane.INFORMATION_MESSAGE);
                a12=JOptionPane.showInputDialog(null,"Please Input a12:","Input the second parameter",JOptionPane.INFORMATION_MESSAGE);
                a21=JOptionPane.showInputDialog(null,"Please Input a21:","Input the third parameter",JOptionPane.INFORMATION_MESSAGE);
                a22=JOptionPane.showInputDialog(null,"Please Input a22:","Input the fourth parameter",JOptionPane.INFORMATION_MESSAGE);
                b1=JOptionPane.showInputDialog(null,"Please Input b1:","Input the fifth parameter",JOptionPane.INFORMATION_MESSAGE);
                b2=JOptionPane.showInputDialog(null,"Please Input b2:","Input the final parameter",JOptionPane.INFORMATION_MESSAGE);
                linearSystem(a11, a12, a21, a22, b1, b2);

                break;

            case 3:
                a3=JOptionPane.showInputDialog(null,"Please Input a:","Input the first parameter",JOptionPane.INFORMATION_MESSAGE);
                b3=JOptionPane.showInputDialog(null,"Please Input b:","Input the second parameter",JOptionPane.INFORMATION_MESSAGE);
                c3=JOptionPane.showInputDialog(null,"Please Input c:","Input the third parameter",JOptionPane.INFORMATION_MESSAGE);
                secondDegreeEquation(a3, b3, c3);
                break;

            case 4:
                break;

            default: JOptionPane.showMessageDialog(null,"Invalid");

        }
        }while(menu!=4);
        System.exit(0);
      
    }
    
}
