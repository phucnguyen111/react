package hust.soict.globalict.aims.media;
import java.util.Arrays;
import java.util.List;
public class Media {
	private String title;
	private String category;
	private float cost; 
	private String id;

	public String getId(){
		return id;
	}
	public void setId(String id){
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
	
    //constructor
    
	Media(String title) {
		
		this.title = title;
	}
	Media(String title, String category){
		this(title);
		this.category = category;
	}
	Media(String title, String category, float cost){
		this.title = title;
		this.category = category;
		this.cost = cost;
	}
	Media(String id, String title, String category, float cost){
		this.title = title;
		this.category = category;
		this.cost = cost;
		this.id = id;
	}

	public boolean search(String title) {
		
		boolean foundToken = false;
		String inputToken[] = title.toLowerCase().split(" ");
		String discTitle[] = this.getTitle().toLowerCase().split(" ");

		List<String> inputTokenList = Arrays.asList(inputToken);
		List<String> discTitleList = Arrays.asList(discTitle);
		
		if(discTitleList.containsAll(inputTokenList)){
			foundToken = true;
		}
		
		return foundToken;
	}
}
