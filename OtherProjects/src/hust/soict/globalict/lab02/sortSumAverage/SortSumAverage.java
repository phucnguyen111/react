package hust.soict.globalict.lab02.sortSumAverage;
import java.util.Scanner;
import java.util.Arrays;
public class SortSumAverage{
    private static Scanner banphim;
    public static void main(String[] args) 
    {
        int n;
        float sum = 0;
        float average;
        banphim = new Scanner(System.in);
        System.out.print("Enter number of elements in the array:");
        n = banphim.nextInt();
        float a[] = new float[n];
        System.out.println("Enter all the elements:");
        for(int i = 0; i < n ; i++)
        {
            a[i] = banphim.nextFloat();
            sum = sum + a[i];
        }
        System.out.println("Sum:"+sum);
        average = (float)sum / n;
        System.out.println("Average:"+average);
        Arrays.sort(a);
        System.out.printf("Sorted Array: %s",Arrays.toString(a));
    }
}