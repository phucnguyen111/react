
import java.util.Scanner;
import java.util.Calendar;
import java.util.Date;

public class MyDate{
    private int day;
    private int strMonth;
    private int year;



    public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return strMonth;
	}

	public void setMonth(int month) {
		this.strMonth = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
    }
    //current date
    MyDate(){
        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        setDay(cal.get(Calendar.DAY_OF_MONTH));
        setMonth(cal.get(Calendar.MONTH));
        setYear(cal.get(Calendar.YEAR));
    }

    //3 paras day month year
    MyDate(int day, int month, int year){
        if(checkDay(day,month,year) == 1){
            setDay(day);
            setMonth(month);
            setYear(year);
        }

    }

    //String para representing a date
    MyDate(String inputDate){
        String[] strDate= null ;
        if(inputDate.contains("-")){
            strDate = inputDate.split("-");
        }
        else if(inputDate.contains("/")){
            strDate = inputDate.split("/");
        }
        else if(inputDate.contains(" ")){
            strDate = inputDate.split(" ");
        }
        else {
            System.out.println("Invalid delimiter !");
        }
        int iMonth= intMonth(strDate[0]);
        int iDay = intDay(strDate[1]);
        int iYear = Integer.parseInt(strDate[2]);
        if(checkDay(iDay, iMonth, iYear) == 1){
        
        setMonth(iMonth);
        setDay(iDay);
        setYear(iYear);
        }
        else {
            System.out.println("Invalid Date ! Run again & enter valid number");
        }
    }

    private int intMonth(String Month){
        if((Month.equals("1") || Month.equals("January") || Month.equals("Jan.") || Month.equals("Jan"))==true){
			return 1;
		}
		else if((Month.equals("2") || Month.equals("February") || Month.equals("Feb.") || Month.equals("Feb"))==true){
			return 2;
		}
		else if((Month.equals("3") || Month.equals("March") || Month.equals("Mar.") || Month.equals("Mar"))==true){
			return 3;
		}
		else if((Month.equals("4") || Month.equals("April") || Month.equals("Apr.") || Month.equals("Apr"))==true){
			return 4;
		}
		else if((Month.equals("5") || Month.equals("May") || Month.equals("May.") || Month.equals("May"))==true){
			return 5;
		}
		else if((Month.equals("6") || Month.equals("June") || Month.equals("Jun.") || Month.equals("Jun"))==true){
			return 6;
		}
		else if((Month.equals("7") || Month.equals("July") || Month.equals("Jul.") || Month.equals("Jul"))==true){
			return 7;
		}
		else if((Month.equals("8") || Month.equals("August") || Month.equals("Aug.") || Month.equals("Aug"))==true){
			return 8;
		}
		else if((Month.equals("9") || Month.equals("September") || Month.equals("Sep.") || Month.equals("Sep"))==true){
			return 9;
		}
		else if((Month.equals("10") || Month.equals("October") || Month.equals("Oct.") || Month.equals("Oct"))==true){
			return 10;
		}
		else if((Month.equals("11") || Month.equals("November") || Month.equals("Nov.") || Month.equals("Nov"))==true){
			return 11;
		}
		else if((Month.equals("12") || Month.equals("December") || Month.equals("Dec.") || Month.equals("Dec"))==true){
			return 12;
        }
        else return 0;
    }

    private int intDay(String Day){
        String strDay = Day;
        if(Day.contains("st")){
            strDay = Day.replace("st", "");
        } 
        else if(Day.contains("nd")){
            strDay = Day.replace("nd", "");
        } 
        else if(Day.contains("rd")){
            strDay = Day.replace("rd", "");
        } 
        else if(Day.contains("th")){
            strDay = Day.replace("th", "");
        } 
        int iDay = Integer.parseInt(strDay);
        return iDay;

    }

    public int checkLeapYear(int year){
		
		if(year % 4 == 0) {
			if (year % 100 == 0) {
				if (year % 400 == 0)
					return 1;
				else
					return 0;
			}
			else
				return 1;
		}else
			return 0;
	}

    public int checkDay(int day, int month, int year){
        if(day < 0 || day > 31 || month <= 0 || month >= 13 || year <= 0){
            return 0;
        }
        else if(checkLeapYear(year) == 0 && month == 2 && day > 28){
            return 0;
        }
        else if((month == 4 || month == 6 || month == 9 || month == 11) && day == 31){
            return 0;
        }
        else return 1;
    }

    public static String accept(){
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter a date ( e.g: Feb 18th 2020 or 2/18/2020 or 2-18-2020): ");
        String inputDate = keyboard.nextLine();
        keyboard.close();
        return inputDate;

    }
 
    public void print(){
        
        System.out.println(this.getDay()+"/"+this.getMonth()+"/"+this.getYear());
    }

}