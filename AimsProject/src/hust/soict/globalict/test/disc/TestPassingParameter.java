package hust.soict.globalict.test.disc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;

public class TestPassingParameter {
	
	public static void main(String[] args) {
		DigitalVideoDisc jungleDVD = new DigitalVideoDisc("Jungle");
		DigitalVideoDisc cinderellaDVD = new DigitalVideoDisc("Cinderella");
		
		swap(jungleDVD, cinderellaDVD);
		System.out.println("Jungle dvd title: " + jungleDVD.getTitle());
		System.out.println("Cinderella dvd title: " + cinderellaDVD.getTitle());
		
		changeTitle(jungleDVD, cinderellaDVD.getTitle());
		System.out.println("Jungle dvd title: " + jungleDVD.getTitle());
	}
	//swap method
	public static void swap( DigitalVideoDisc obj1, DigitalVideoDisc obj2) {
	  String title;

	  title = obj1.getTitle();
	  obj1.setTitle(obj2.getTitle());
	  obj2.setTitle(title);
	  
	 
	}
	
	public static void changeTitle(DigitalVideoDisc dvd, String title) {
		String old_title = dvd.getTitle();
		dvd.setTitle(title);
		dvd = new DigitalVideoDisc(old_title);
	}
}
