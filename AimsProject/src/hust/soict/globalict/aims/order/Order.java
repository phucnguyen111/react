package hust.soict.globalict.aims.order;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;

import hust.soict.globalict.aims.utils.MyDate;
import java.lang.Math; 
import java.util.ArrayList;
import java.util.List;



public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();


	public MyDate dateOrdered;

	public static final int MAX_LIMITED_ORDERS = 5;

	private static int nbOrders = 0;

	
	public int getNbOrders()
	{
		return nbOrders;
	}
	public void setNbOrders(int nbOrders) {
		Order.nbOrders = nbOrders;
	}

	public Order(){
		dateOrdered = new MyDate();
		if(nbOrders == MAX_LIMITED_ORDERS) {
			System.out.println("Max Orders reached ! Cannot add more orders");
		} 
		else{
			setNbOrders(getNbOrders() + 1);
		}
	}
	
	public void addMedia(Media media){
		if(itemsOrdered.size() < MAX_NUMBERS_ORDERED){
			itemsOrdered.add(media);
		}
		else if(itemsOrdered.size() >= MAX_NUMBERS_ORDERED){
			System.out.println("The maximum number of items reached ( 10 items ) ! Cannot add "+"\'"+media.getTitle()+"\'"+" to your order");
		}
	}
	public boolean removeMedia(String id){
		boolean check = false;
		if(itemsOrdered.size() > 0 ){
			for(int i = 0; i < itemsOrdered.size();i++){
				if(itemsOrdered.get(i).getId().equals(id) == true){
					itemsOrdered.remove(i);
					check = true;
				}
				
			}
		}
		else if(itemsOrdered.size() <= 0){
			System.out.println("\nNo items in order to remove\n");
		}
		return check;

	}

	public float totalCost(){
		float cost = 0.0f;
		for(int i = 0; i < itemsOrdered.size(); i++){
			cost += itemsOrdered.get(i).getCost();
		}
		return cost;
	}
	public void addMediaDVD(Media media){
		if(itemsOrdered.size() < MAX_NUMBERS_ORDERED){
		
				DigitalVideoDisc dvd = new DigitalVideoDisc(media.getId(),media.getTitle(), media.getCategory(), media.getCost());
				if(itemsOrdered.contains(dvd) == false){
					itemsOrdered.add(dvd);
				}
		}
		else if(itemsOrdered.size() >= MAX_NUMBERS_ORDERED){
			System.out.println("The maximum number of items reached ( 10 items ) ! Cannot add "+"\'"+media.getTitle()+"\'"+" to your order");
		}
	}
	public void addMediaList( List<Media> MediaList) {
		for (int i = 0; i < MediaList.size(); i++) {
			addMediaDVD(MediaList.get(i));
		}
	}

	public void printOrder() {
		int stt = 1;
		Media freeItem = getALuckyItem();
		System.out.println("\n****************************************Your Order********************************************");
		dateOrdered.printDateOrdered();
		System.out.println("______________________________________________________________________________________________");
		System.out.format("\n%-5s%-15s%-25s%-20s%-15s\n","No","Id", "Title", "Category","Cost($)");
		System.out.println("______________________________________________________________________________________________\n");
		
		for(Media media: itemsOrdered) {
			System.out.format("%-5d%-15s%-25s%-20s%-15.2f\n",stt, media.getId(),media.getTitle(),media.getCategory(),media.getCost() );
			stt++;
		}
		System.out.println("______________________________________________________________________________________________\n");
		System.out.println("\n**********************************************************************************************");


		System.out.println("\nThe total number of items in your order is  " + itemsOrdered.size());
		System.out.println("\nCongratulations ! You get "+"\'"+freeItem.getTitle()+"\'"+" for free.");
		System.out.println("\nYour total cost is: "+"$"+ (totalCost()-freeItem.getCost()));
		System.out.println("\nThanks for shopping !\n");
		
	}
	
	public Media getALuckyItem(){
		int randomItemindex = -1; 
	
		if(itemsOrdered.size()<=0){
			System.out.println("No items in your order so no free items !");
		}
		else if(itemsOrdered.size() >=2){
			for(int i = 0; i<itemsOrdered.size(); i++){
				 randomItemindex = (int)(Math.random()*itemsOrdered.size());
			}
		}
		else if(itemsOrdered.size() == 1){
			System.out.println("You have only 1 items. Please buy 2 or more to get 1 for free !");
		}
		return itemsOrdered.get(randomItemindex);
	}
	
	
	
}
