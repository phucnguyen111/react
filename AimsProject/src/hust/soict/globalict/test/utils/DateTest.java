package hust.soict.globalict.test.utils;
import hust.soict.globalict.aims.utils.DateUtils;
import hust.soict.globalict.aims.utils.MyDate;

public class DateTest{
    public static void main(String[] args){
        MyDate currentDate = new MyDate();
        //current date February 29th 2020 format
        currentDate.print();
    
        // String Date MyDate(String day,String month,String year) from 0 to 9999 year name in format pronouncing every two number of year
        MyDate iDate = new MyDate("thirty-two","December","twenty zero");               // 31/12/2000 => Invalid
        MyDate iDate2 = new MyDate("thirtieth","July","ninety-seven sixty-three");      // 30/7/9763
        MyDate iDate3 = new MyDate("twenty-fourth","August","nineteen sixty-nine");     // 24/8/1969
        MyDate iDate4 = new MyDate("twenty-eighth","January","forty-six five");         // 28/1/4605
        MyDate iDate5 = new MyDate("first","November","nineteen ninety-nine");          // 1/11/1999
        MyDate iDate6 = new MyDate("eleventh","March","twenty twenty");                 // 11/3/2020
        MyDate iDate7 = new MyDate("thirty-first","April","eighteen ten");              // 31/4/1810 => Invalid
        MyDate iDate8 = new MyDate("twentieth","May","twenty eleven");                  // 20/5/2011
        MyDate iDate9 = new MyDate("fifth","June","thirty-eight seventy-seven");        // 5/6/3877
        MyDate iDate10 = new MyDate("seventeenth","September","ten eleven");            // 17/9/1011
        MyDate iDate11 = new MyDate("thirty-first","October","ninety-nine ninety-nine");// 31/10/9999 
        MyDate iDate12 = new MyDate("twenty-ninth","February","twenty nineteen");       // 29/2/2019 => Invalid
        MyDate iDate13 = new MyDate("twenty-ninth","February","twenty twenty");         // 29/2/2020
        MyDate iDate14 = new MyDate("twenty-second","January","ten zero");              // 22/1/1000
        MyDate iDate15 = new MyDate("ninth","December","nine ninety-nine");             // 9/12/999
        MyDate iDate16 = new MyDate("second","November","one zero");                    // 2/11/100
        MyDate iDate17 = new MyDate("twelfth","January","six");                         // 12/1/6
        MyDate iDate18 = new MyDate("fifteenth","June","zero");                         // 15/6/0
        MyDate iDate19 = new MyDate("fourteenth","July","eleven");                      // 14/7/11
        MyDate iDate20 = new MyDate("third","August","eighty-five");                    // 3/8/85
        MyDate iDate21 = new MyDate("nineteenth","September","forty");                  // 19/9/40
        System.out.println("This is some dates converted from their names to dd/mm/yyyy format: ");
        iDate.printDate();
        
        iDate2.printDate();
        iDate3.printDate();
        iDate4.printDate();
        iDate5.printDate();
        iDate6.printDate();
        iDate7.printDate();
        iDate8.printDate();
        iDate9.printDate();
        iDate10.printDate();
        iDate11.printDate();
        iDate12.printDate();
        iDate13.printDate();
        iDate14.printDate();
        iDate15.printDate();
        iDate16.printDate();
        iDate17.printDate();
        iDate18.printDate();
        iDate19.printDate();
        iDate20.printDate();
        iDate21.printDate();
        

        //User enter Date format eg Feb 19th 2020 print out my chosen format dd/mm/yyyy
        String inputDate = MyDate.accept();
        MyDate date = new MyDate(inputDate);
        if(date.checkDay(date.getDay(),date.getMonth(),date.getYear()) == 1){
        System.out.println("Date in dd/mm/yyyy format: ");
        date.printDate();
        }
        
        MyDate Date1 = new MyDate(1,11,1999);
        MyDate Date2 = new MyDate(31,11,1999); // Invalid
        MyDate Date3 = new MyDate(31,12,2019);
        MyDate Date4 = new MyDate(1,1,2020);
        MyDate Date5 = new MyDate(29,2,2020);
        MyDate Date6 = new MyDate(24,8,1969);
        MyDate Date7 = new MyDate(1,3,2020);
        MyDate Date8 = new MyDate(26,3,2020);
        MyDate Date9 = new MyDate(28,1,1962);
        MyDate Date10 = new MyDate(31,10,2019);
        MyDate Date11 = new MyDate(29,2,1999); // Invalid
        MyDate Date12 = new MyDate(2,11,1999);
        MyDate Date13 = new MyDate(1,11,1999);



        MyDate [] Dates = new MyDate [] {Date1,Date2,Date3,Date4,Date5,Date6,Date7,Date8,Date9,Date10};

        System.out.println("\nCompare two dates:");
        DateUtils util = new DateUtils();
        util.printResult(Date2, Date1);
        util.printResult(Date1, Date11);
        util.printResult(Date3, Date4);
        util.printResult(Date12, Date1);
        util.printResult(Date1, Date13);
        
        System.out.println("\nSort dates in ascending order:");
        util.sortDates(Dates);
        for(int i = 0 ; i < Dates.length; i++){
            Dates[i].printDate();
        }

    }
}