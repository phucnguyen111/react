package hust.soict.globalict.garbage.concatenation;

import java.util.Random;

public class ConcatenationInLoops{
    public static void main(String[] args){
        Random r = new Random(500);
        long start = System.currentTimeMillis();
        String phuc = "OOLT.20192.NguyenManhPhuc.20176845 Hello";
        for(int i = 0; i < 65536; i++){
            phuc = phuc + r.nextInt(2);
        }
        System.out.println(System.currentTimeMillis() - start);

        r = new Random(123);
        start = System.currentTimeMillis();
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < 65536; i++){
            sb.append(r.nextInt(2));
        }
        phuc = sb.toString();
        System.out.println(System.currentTimeMillis() - start);

        r = new Random(123);
        start = System.currentTimeMillis();
        StringBuffer sbf = new StringBuffer();
        for(int i = 0; i < 65536; i++){
            sbf.append(r.nextInt(2));
        }
        phuc = sbf.toString();
        System.out.println(System.currentTimeMillis() - start);
    }
}
