
public class Aims {

	public static void main(String[] args) {
		Order anOrder =new Order();
		
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("Toy Story 4","Animation","Josh Cooley",100,11.97f);
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("The Hunger Games","Science Fiction","Gary Ross",142,20.14f);
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Parasite","Black Comedy","Bong Joon-ho",132,18.53f);
		DigitalVideoDisc dvd4 = new DigitalVideoDisc("Star Wars","Sciecne Fiction","George Lucas",124,29.69f);

		anOrder.addDigitalVideoDisc(dvd1);
		anOrder.addDigitalVideoDisc(dvd2);
		anOrder.addDigitalVideoDisc(dvd3);
		anOrder.addDigitalVideoDisc(dvd4);
		
		anOrder.printOrder();
		anOrder.removeDigitalVideoDisc(dvd1);
		anOrder.printOrder();
		anOrder.removeDigitalVideoDisc(dvd4);
		anOrder.printOrder();
		
		
		

	}

}
