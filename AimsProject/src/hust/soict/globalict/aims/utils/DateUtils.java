package hust.soict.globalict.aims.utils;

public class DateUtils{
   

    public int CompareTwoDates(MyDate date1, MyDate date2){
        if(date1.getYear() > date2.getYear()){
            return  1; 
        }
        else if(date1.getYear()<date2.getYear()){
            return  -1;
        }
        else{
            if(date1.getMonth() > date2.getMonth()){
                return  1;
            }
            else if(date1.getMonth() < date2.getMonth()){
                return  -1;
            }
            else{
                if(date1.getDay() > date2.getDay()){
                    return  1;
                }
                else if(date1.getDay() < date2.getDay()){
                    return  -1;
                }
                else return  0;
            }
        
        }
        
    }

    public void sortDates(MyDate [] dates){
        for(int i = 0; i < dates.length; i++){
            for(int j = i+1; j < dates.length; j++){
                if(CompareTwoDates(dates[i], dates[j]) == 1){
                    MyDate temp = dates[i];
                    dates[i] = dates[j];
                    dates[j] = temp;
                }
            }
        }
    }

    public void printResult(MyDate d1, MyDate d2){
        if(d1.getDay() != 0 && d1.getMonth() != 0 && d1.getYear()!=0  && d2.getDay() != 0 && d2.getMonth() != 0 && d2.getYear()!=0 ){
            if(CompareTwoDates(d1,d2) == 1 ){
                System.out.println(d1.getDay()+"/"+d1.getMonth()+"/"+d1.getYear()+" is after "+d2.getDay()+"/"+d2.getMonth()+"/"+d2.getYear()+"\n");
            }
            else if(CompareTwoDates(d1,d2) == -1){
                System.out.println(d1.getDay()+"/"+d1.getMonth()+"/"+d1.getYear()+" is before "+d2.getDay()+"/"+d2.getMonth()+"/"+d2.getYear()+"\n");
            }
            else if(CompareTwoDates(d1, d2) == 0){
                System.out.println("Two dates are the same\n");
            }
        }
        else{
            System.out.println("Invalid Date");
        }
        
        
    }

}