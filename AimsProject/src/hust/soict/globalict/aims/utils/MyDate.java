package hust.soict.globalict.aims.utils;

import java.util.Scanner;
import java.util.Calendar;
import java.util.Date;
import java.util.Arrays;
import java.util.List;


public class MyDate{
    private int day;
    private int strMonth;
    private int year;



    public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return strMonth;
	}

	public void setMonth(int month) {
		this.strMonth = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
    }
    //current date
    public MyDate(){
        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        setDay(cal.get(Calendar.DAY_OF_MONTH));
        setMonth(cal.get(Calendar.MONTH)+1);
        setYear(cal.get(Calendar.YEAR));
    }

    //3 paras day month year
    public MyDate(int day, int month, int year){
        if(checkDay(day,month,year) == 1){
            setDay(day);
            setMonth(month);
            setYear(year);
        }

    }
    
    String[] dayName = {"first","second","third","fourth","fifth","sixth","seventh","eighth","ninth","tenth","eleventh","twelfth","thirteenth"
                        ,"fourteenth","fifteenth" ,"sixteenth"	,"seventeenth" ,"eighteenth" ,"nineteenth",  "twentieth","twenty-first",
                        "twenty-second","twenty-third","twenty-fourth","twenty-fifth","twenty-sixth","twenty-seventh","twenty-eighth","twenty-ninth",
                        "thirtieth","thirty-first"};
    String[] monthName1 = {"January","February","March","April","May","June","July","August","September","October","November","December"};

    String[] yearName1 = {"nineteen","twenty"};
    String[] yearName2 = {"ten","twenty","thirty","forty","fifty","sixty","seventy","eighty","ninety"};
    String[] yearName3 = {"eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen"};
    String[] yearName4 = {"zero","one","two","three","four","five","six","seven","eight","nine"};

    List<String> dayList = Arrays.asList(dayName);
    List<String> monthList = Arrays.asList(monthName1);
    List<String> yearList1 = Arrays.asList(yearName1);
    List<String> yearList2 = Arrays.asList(yearName2);
    List<String> yearList3 = Arrays.asList(yearName3);
    List<String> yearList4 = Arrays.asList(yearName4);

    //overloading constructors method String instead of Int
    public MyDate(String day, String month, String year){
        if(dayList.contains(day) && monthList.contains(month) && 
            ( yearList1.parallelStream().anyMatch(year::contains) || yearList2.parallelStream().anyMatch(year::contains) 
            ||yearList3.parallelStream().anyMatch(year::contains) || yearList4.parallelStream().anyMatch(year::contains) )){
            int monthInt = intMonth(month);
            int dayInt = 0;
            for(int i = 0; i < 31; i++){
                if(dayName[i].equals(day) == true){
                    dayInt = i+1;
                }
            }
            int yearInt = intYearName(year);
            if(checkDay(dayInt, monthInt, yearInt) == 1){
            setDay(dayInt);
            setMonth(monthInt);
            setYear(yearInt);
            }
            
        }
        
    }

    //String para representing a date
    public MyDate(String inputDate){
        String[] strDate= null ;
        if(inputDate.contains("-")){
            strDate = inputDate.split("-");
        }
        else if(inputDate.contains("/")){
            strDate = inputDate.split("/");
        }
        else if(inputDate.contains(" ")){
            strDate = inputDate.split(" ");
        }
        else {
            System.out.println("Invalid delimiter !");
        }
        int iMonth= intMonth(strDate[0]);
        int iDay = intDay(strDate[1]);
        int iYear = Integer.parseInt(strDate[2]);
        if(checkDay(iDay, iMonth, iYear) == 1){
        
        setMonth(iMonth);
        setDay(iDay);
        setYear(iYear);
        }
        else {
            System.out.println("Invalid Date ! Run again & enter valid number");
        }
    }

    private int intYearName(String year){
        String[] strYear1 = null;
        String[] strYear2 ;
        String[] strYear3 ;
        String[] strYear4;
        int y12 = 0;
        int y11 = 0;
        int y1 = 0;
        int y2 = 0;
        int y3 = 0;
        
        
        if(year.contains(" ")){
            strYear1 = year.split(" ");
            if(strYear1[0].contains("-") == true){
                strYear3 = strYear1[0].split("-");
                for(int i = 1; i < 9;i++){
                    if(yearName2[i].equals(strYear3[0])){
                        y11 = (i+1)*1000;
                    }
                }
                for(int j = 0; j < 10; j++){
                    if(yearName4[j].equals(strYear3[1])){
                        y12 = j*100;
                    }
                }
                y1 = y11 + y12;
            }
            else{
                
                for(int i = 0; i < 9; i++){
                    if(yearName3[i].equals(strYear1[0])){
                        y1 = (i+11)*100;
                    }
                }
                for(int j = 0;j < 9;j++){
                    if(yearName2[j].equals(strYear1[0])){
                        y1 = (j+1)*1000;
                    }
                }
                for(int k = 0;k < 10;k++){
                    if(yearName4[k].equals(strYear1[0])){
                        y1 = k*100;
                    }
                }
            }
           
            if(strYear1[1].contains("-") == true){
                strYear2 = strYear1[1].split("-");
                for(int i = 1;i < 9;i++){
                    if(yearName2[i].equals(strYear2[0])){
                        y2 = (i+1)*10;
                    }
                }
                for(int j = 0;j < 10; j++){
                    if(yearName4[j].equals(strYear2[1])){
                        y3 = j;
                    }
                }
            }
            else{
                for(int i = 0;i < 9; i++){
                    if(yearName2[i].equals(strYear1[1])){
                        y2 = (i+1)*10;
                    }
                    if(yearName3[i].equals(strYear1[1])){
                        y2 = i+11;
                    }
                }
                for(int j = 0;j < 10;j++){
                    if(yearName4[j].equals(strYear1[1])){
                        y2 = j;
                    }
                }
                
            }
        }
        else if(year.contains(" ") == false){
            if(year.contains("-")){
                strYear4 = year.split("-");
                for(int i = 0;i < 9;i++){
                    if(yearName2[i].equals(strYear4[0])){
                        y11 = (i+1)*10;
                    }
                }
                for(int j = 1; j < 10;j++){
                    if(yearName4[j].equals(strYear4[1])){
                        y12 = j;
                    }
                }
                y1 = y11 + y12;
            }
            else{
                for(int i = 0; i < 10; i++){
                    if(yearName4[i].equals(year)){
                        y1 = i;
                    }
                }
                for(int j = 0; j < 9; j++){
                    if(yearName3[j].equals(year)){
                        y1 = (j+11);
                    }
                }
                for(int k = 0; k < 9; k++){
                    if(yearName2[k].equals(year)){
                        y1 = (k+1)*10;
                    }
                }
            }
        }
        else{
            System.out.println("Invalid Year name!");
        }
        int y = y1+y2+y3;

        return y;
        
    }

    private int intMonth(String Month){
        if((Month.equals("1") || Month.equals("January") || Month.equals("Jan.") || Month.equals("Jan"))==true){
			return 1;
		}
		else if((Month.equals("2") || Month.equals("February") || Month.equals("Feb.") || Month.equals("Feb"))==true){
			return 2;
		}
		else if((Month.equals("3") || Month.equals("March") || Month.equals("Mar.") || Month.equals("Mar"))==true){
			return 3;
		}
		else if((Month.equals("4") || Month.equals("April") || Month.equals("Apr.") || Month.equals("Apr"))==true){
			return 4;
		}
		else if((Month.equals("5") || Month.equals("May") || Month.equals("May.") || Month.equals("May"))==true){
			return 5;
		}
		else if((Month.equals("6") || Month.equals("June") || Month.equals("Jun.") || Month.equals("Jun"))==true){
			return 6;
		}
		else if((Month.equals("7") || Month.equals("July") || Month.equals("Jul.") || Month.equals("Jul"))==true){
			return 7;
		}
		else if((Month.equals("8") || Month.equals("August") || Month.equals("Aug.") || Month.equals("Aug"))==true){
			return 8;
		}
		else if((Month.equals("9") || Month.equals("September") || Month.equals("Sep.") || Month.equals("Sep"))==true){
			return 9;
		}
		else if((Month.equals("10") || Month.equals("October") || Month.equals("Oct.") || Month.equals("Oct"))==true){
			return 10;
		}
		else if((Month.equals("11") || Month.equals("November") || Month.equals("Nov.") || Month.equals("Nov"))==true){
			return 11;
		}
		else if((Month.equals("12") || Month.equals("December") || Month.equals("Dec.") || Month.equals("Dec"))==true){
			return 12;
        }
        else return 0;
    }

    private int intDay(String Day){
        String strDay = Day;
        if(Day.contains("st")){
            strDay = Day.replace("st", "");
        } 
        else if(Day.contains("nd")){
            strDay = Day.replace("nd", "");
        } 
        else if(Day.contains("rd")){
            strDay = Day.replace("rd", "");
        } 
        else if(Day.contains("th")){
            strDay = Day.replace("th", "");
        } 
        int iDay = Integer.parseInt(strDay);
        return iDay;

    }

    


    public int checkLeapYear(int year){
		
		if(year % 4 == 0) {
			if (year % 100 == 0) {
				if (year % 400 == 0)
					return 1;
				else
					return 0;
			}
			else
				return 1;
		}else
			return 0;
	}

    public int checkDay(int day, int month, int year){
        if(day <= 0 || day > 31 || month <= 0 || month >= 13 || year < 0){
            return 0;
        }
        else if(checkLeapYear(year) == 0 && month == 2 && day > 28){
            return 0;
        }
        else if((month == 4 || month == 6 || month == 9 || month == 11) && day == 31){
            return 0;
        }
        else return 1;
    }

    public static String accept(){
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter a date ( e.g: Feb 18th 2020 or 2/18/2020 or 2-18-2020): ");
        String inputDate = keyboard.nextLine();
        keyboard.close();
        return inputDate;

    }


    String[] monthName2 = {"January","February","March","April","May","June","July","August","September","October","November","December"};
    //print current date with February 29th format
    public void print(){
        String month = monthName2[this.getMonth()-1];
        String day;
        if(this.getDay() == 1 || this.getDay() == 21 || this.getDay() == 31){
            day = this.getDay()+"st";
        }
        else if(this.getDay() == 2 || this.getDay() == 22 ){
            day = this.getDay()+"nd";
        }
        else if(this.getDay() == 3 || this.getDay() == 23){
            day = this.getDay()+"rd";
        }
        else{
            day = this.getDay()+"th";
        }
        System.out.println("The current date is: "+month+" "+day+" "+this.getYear());
    }

    /*
    public void print(){
        
        System.out.println(this.getDay()+"/"+this.getMonth()+"/"+this.getYear());
    }
    */
    //print date with string input
    public void printDate(){
        if(checkDay(this.getDay(),this.getMonth(),this.getYear()) == 0){
            System.out.println("Invalid Date");
        }
        else{
            System.out.println(this.getDay()+"/"+this.getMonth()+"/"+this.getYear());
        }
    }

    public void printDateOrdered(){
        
        System.out.println("Date: "+this.getDay()+"/"+this.getMonth()+"/"+this.getYear());
    }


}