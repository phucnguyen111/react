package hust.soict.globalict.lab02.addTwoMatrices;
import java.util.Scanner;
public class AddTwoMatrices{

    private static Scanner banphim;
 public static void main(String[] args)
   {
      int row, col, a, b;
      banphim = new Scanner(System.in);
 
      System.out.println("Input number of rows of matrix");
      row = banphim.nextInt();
      System.out.println("Input number of columns of matrix");
      col  = banphim.nextInt();
 
      float array1[][] = new float[row][col];
      float array2[][] = new float[row][col];
      float sum[][] = new float[row][col];
 
      System.out.println("Input elements of first matrix");
 
      for (  a = 0 ; a < row ; a++ )
         for ( b = 0 ; b < col ; b++ )
            array1[a][b] = banphim.nextFloat();
 
      System.out.println("Input the elements of second matrix");
 
      for ( a = 0 ; a < row ; a++ )
         for ( b = 0 ; b < col ; b++ )
            array2[a][b] = banphim.nextFloat();
 
      for ( a = 0 ; a < row ; a++ )
         for ( b = 0 ; b < col ; b++ )
             sum[a][b] = array1[a][b] + array2[a][b]; 
 
      System.out.println("Sum of the 2 matrices is:");
 
      for ( a = 0 ; a < row ; a++ )
      {
         for ( b = 0 ; b < col ; b++ )
            System.out.print(sum[a][b]+"\t");
 
         System.out.println();
      }
      System.exit(0);  
   }
}
